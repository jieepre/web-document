#!/bin/bash
cd vue-web || exit
yarn build
cd ../vue-admin || exit
yarn build:prod
cd ..
docker build -f docker/Dockerfile-web -t jieepre/web:20200615 .
