module.exports = {

  /* 代码保存时进行eslint检测 */
  lintOnSave: true,
  /* 是否在构建生产包时生成 sourceMap 文件，false将提高构建速度 */
  productionSourceMap: false,
  publicPath: '/',
  outputDir: 'dist',
  assetsDir: 'assets',
  /* webpack-dev-server 相关配置 */
  devServer: {
    /* 自动打开浏览器 */
    open: false,
    /* 设置为0.0.0.0则所有的地址均能访问 */
    host: '0.0.0.0',
    disableHostCheck: true,
    port: 8080,
    https: false,
    hotOnly: false,
    /* 使用代理配置跨域*/
    proxy: {
      '/api': {
        target: 'http://localhost:8590', // target host
        changeOrigin: true, // needed for virtual hosted sites
        ws: true, // proxy websockets
        pathRewrite: {
          '^/api': ''
        },
        router: {
          // when request.headers.host == 'dev.localhost:3000',
          // override target 'http://www.example.org' to 'http://localhost:8000'
          'dev.localhost:3000': 'http://localhost:8000'
        }
      }
    }
  }
}
