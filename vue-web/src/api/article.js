import request from '@/utils/request'

export function getList(data) {
  return request({
    url: '/api/web/article/list/' + data.page + '/' + data.size,
    method: 'get'
  })
}

export function articleSearch(data) {
  return request({
    url: '/api/web/article/search/' + data.page + '/' + data.size + '/' + data.title,
    method: 'get'
  })
}

export function articleCategoriesList(data) {
  return request({
    url: '/api/web/article/categories/' + data.category + '/' + data.page + '/' + data.size,
    method: 'get'
  })
}

export function articleTagsList(data) {
  return request({
    url: '/api/web/article/tags/' + data.tag + '/' + data.page + '/' + data.size,
    method: 'get'
  })
}

export function detailArticle(id) {
  return request({
    url: '/api/web/article/detail/' + id,
    method: 'get'
  })
}

export function searchArticle(data) {
  return request({
    url: '/api/v1/document/article/search',
    method: 'post',
    data
  })
}

export function articleNews() {
  return request({
    url: '/api/web/articles/news',
    method: 'get'
  })
}

export function archivesNews() {
  return request({
    url: '/api/web/archives/news',
    method: 'get'
  })
}

export function articleItem() {
  return request({
    url: '/api/web/total',
    method: 'get'
  })
}
