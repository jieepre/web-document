import request from '@/utils/request'

export function tagsList() {
  return request({
    url: '/api/web/tags',
    method: 'get'
  })
}
