import request from '@/utils/request'

export function detailType(id) {
  return request({
    url: '/api/v1/document/type/detail/' + id,
    method: 'get'
  })
}

export function categoriesList(data) {
  return request({
    url: '/api/web/categories/list/' + data.page + '/' + data.size,
    method: 'get'
  })
}

export function categoriesNews() {
  return request({
    url: '/api/web/categories/news',
    method: 'get'
  })
}
