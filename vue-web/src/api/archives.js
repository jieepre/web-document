import request from '@/utils/request'

export function archivesList(data) {
  return request({
    url: '/api/web/archives/list/' + data.page + '/' + data.size,
    method: 'get'
  })
}

export function archivesConditionList(data) {
  return request({
    url: '/api/web/archives/list/' + data.condition + '/' + data.page + '/' + data.size,
    method: 'get'
  })
}
