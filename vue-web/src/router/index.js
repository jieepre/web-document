import Vue from 'vue'
import VueRouter from 'vue-router'
import Layout from '../layout/index.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/404',
    component: () => import('@/views/404.vue'),
    hidden: true
  },
  {
    path: '/',
    component: Layout,
    redirect: '',
    children: [
      {
        name: 'home',
        path: '',
        component: () => import('@/views/home/index.vue'),
        meta: {
          title: '小助理'
        }
      }
    ]
  },
  {
    path: '/',
    component: Layout,
    redirect: 'about',
    children: [{
      path: 'about',
      name: 'about',
      component: () => import('@/views/about/index.vue'),
      meta: {
        title: '关于'
      }
    }]
  },
  {
    path: '/',
    component: Layout,
    redirect: 'categories',
    children: [{
      path: 'categories',
      name: 'categories',
      component: () => import('@/views/categories/index.vue'),
      meta: {
        title: '分类'
      }
    }]
  }, {
    path: '/',
    component: Layout,
    redirect: 'categories/:categories',
    children: [{
      path: 'categories/:categories',
      name: 'categories:category',
      component: () => import('@/views/home/index.vue'),
      meta: {
        title: '分类'
      }
    }]
  },
  {
    path: '/',
    component: Layout,
    redirect: 'archives',
    children: [{
      name: 'archives',
      path: 'archives',
      component: () => import('@/views/archives/index.vue'),
      meta: {
        title: '归档'
      }
    }]
  }, {
    path: '/',
    component: Layout,
    redirect: 'archives/:archives',
    children: [{
      name: 'archives:archive',
      path: 'archives/:archives',
      component: () => import('@/views/archives/index.vue'),
      meta: {
        title: '归档'
      }
    }]
  },
  {
    path: '/',
    component: Layout,
    redirect: 'tags',
    children: [{
      name: 'tags',
      path: 'tags',
      component: () => import('@/views/tags/index.vue'),
      meta: {
        title: '标签'
      }
    }]
  }, {
    path: '/',
    component: Layout,
    redirect: 'tags/:tags',
    children: [{
      path: 'tags/:tags',
      name: 'tags:tag',
      component: () => import('@/views/home/index.vue'),
      meta: {
        title: '标签'
      }
    }]
  }, {
    path: '/posts/:id(\\w{8})',
    name: 'detail',
    component: () => import('@/views/detail/index.vue'),
    meta: {
      title: ''
    }
  }, {
    path: '/',
    component: Layout,
    redirect: 'search',
    children: [{
      path: 'search',
      name: 'search',
      component: () => import('@/views/search/index.vue'),
      meta: {
        title: '查询'
      }
    }]
  },
  {path: '*', redirect: '/404', hidden: true}
]

const router = new VueRouter({
  mode: 'history',
  // mode: 'hash',
  base: '/',
  routes,
  scrollBehavior: () => ({y: 0})
})
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}
export default router
