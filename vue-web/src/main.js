import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './assets/css/index.css'
import './assets/js/click_heart.js'
import mavonEditor from 'mavon-editor'
import 'mavon-editor/dist/css/index.css'
import '@/utils/ribbon.min.js'
Vue.use(mavonEditor)
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
// 解决路由导航返回到顶部
router.beforeEach((to, from, next) => {
  // chrome
  document.body.scrollTop = 0
  // firefox
  document.documentElement.scrollTop = 0
  // safari
  window.pageYOffset = 0
  if (window._hmt) {
    if (to.path) {
      window._hmt.push(['_trackPageview', '/' + to.fullPath])
    }
  }
  next()
})
