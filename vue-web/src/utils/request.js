import axios from 'axios'
// create an axios instance
const service = axios.create({
  baseURL: process.env.NODE_ENV === 'production' ? process.env.VUE_APP_BASE_API : 'http://localhost:8590', // api 的 base_url
  timeout: 5000 // request timeout
})

export default service
