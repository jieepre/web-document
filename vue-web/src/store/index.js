import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    sidebar: 'close'
  },
  mutations: {
    open(state) {
      state.sidebar = 'open'
    },
    close(state) {
      state.sidebar = 'close'
    }

  },
  actions: {
    open(state) {
      state.sidebar = 'open'
    },
    close(state) {
      state.sidebar = 'close'
    }

  },
  getters: {
    open: state => {
      return state.sidebar
    }
  }
})
