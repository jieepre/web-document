## 项目名称
 - 文档管理
 
 ## 运行
进入 docker 目录 运行 `docker-compose up -d`
需要修改 `nginx.conf application.yml`配置文件 ip 为自己本机IP地址 `192.168.1.254` 
 ## 构建镜像
 
```cmd
docker build -f ../docker/Dockerfile -t document:20200615 .
```
