import JSEncrypt from 'jsencrypt/bin/jsencrypt'

// 密钥对生成 http://web.chacuo.net/netrsakeypair

const publicKey = 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDYx8Qt6Qpp4ygW5prP2679Axjy\n' +
  'Wbzk0bYbp5rtG4p3CW3K8FEe8qhB5Z2qtdLLhOFWWTFGcyGkNC/uJhBR9nzZoCab\n' +
  'CE7QuZsXB2QCiyPi1VXOIz9Le/LsAEz0WKVDJs5Kx55WIX8zo+M84I8pFPEwcjpa\n' +
  'o8cqL2L3aGafAifuLwIDAQAB'

const privateKey = ''

// 加密
export function encrypt(txt) {
  const encrypt = new JSEncrypt()
  // 设置公钥
  encrypt.setPublicKey(publicKey)
  // 对需要加密的数据进行加密
  return encrypt.encrypt(txt)
}

// 解密
export function decrypt(txt) {
  const encryptor = new JSEncrypt()
  encryptor.setPrivateKey(privateKey)
  return encryptor.decrypt(txt)
}
