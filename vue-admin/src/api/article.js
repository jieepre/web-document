import request from '@/utils/request'

export function saveArticle(data) {
  return request({
    url: '/api/v1/document/article/save',
    method: 'post',
    data
  })
}

export function updateArticle(data) {
  return request({
    url: '/api/v1/document/article/update',
    method: 'put',
    data
  })
}

export function getList(data) {
  return request({
    url: '/api/v1/document/article/list/' + data.page + '/' + data.size,
    method: 'get',
    params: { status: data.status }
  })
}

export function detailArticle(id) {
  return request({
    url: '/api/v1/document/article/detail/' + id,
    method: 'get'
  })
}

export function recoverArticle(id) {
  return request({
    url: '/api/v1/document/article/recover/' + id,
    method: 'delete'
  })
}

export function deleteArticle(id) {
  return request({
    url: '/api/v1/document/article/delete/' + id,
    method: 'delete'
  })
}

export function searchArticle(data) {
  return request({
    url: '/api/v1/document/article/search',
    method: 'post',
    data
  })
}

export function topArticle(params) {
  return request({
    url: '/api/v1/document/article/top/' + params.id,
    method: 'post',
    params
  })
}

export function countFiltrate() {
  return request({
    url: '/api/v1/document/article/total',
    method: 'get'
  })
}

export function uploadImage(data) {
  return request({
    url: '/api/v1/upload/images',
    method: 'post',
    headers: { 'Content-Type': 'multipart/form-data' },
    data
  })
}
