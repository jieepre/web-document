import request from '@/utils/request'

export function saveType(data) {
  return request({
    url: '/api/v1/document/type/save',
    method: 'post',
    data
  })
}

export function updateType(data) {
  return request({
    url: '/api/v1/document/type/update',
    method: 'put',
    data
  })
}

export function deleteType(id) {
  return request({
    url: '/api/v1/document/type/delete/' + id,
    method: 'delete'
  })
}

export function detailType(id) {
  return request({
    url: '/api/v1/document/type/detail/' + id,
    method: 'get'
  })
}

export function getList() {
  return request({
    url: '/api/v1/document/type/list/1/100',
    method: 'get'
  })
}
