import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/api/v1/user/login',
    method: 'post',
    data
  })
}

export function updatePassword(data) {
  return request({
    url: '/api/v1/user/update_password',
    method: 'post',
    data
  })
}

export function getInfo(token) {
  return request({
    url: '/api/v1/user/info',
    method: 'get',
    params: { token }
  })
}

export function logout() {
  return request({
    url: '/api/v1/user/logout',
    method: 'post'
  })
}

export function getList(data) {
  return request({
    url: '/api/v1/user/list/' + data.page + '/' + data.size,
    method: 'get'
  })
}










