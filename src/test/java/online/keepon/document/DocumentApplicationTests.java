package online.keepon.document;

import com.alibaba.fastjson.JSONObject;
import online.keepon.document.mapper.ArticleMapper;
import online.keepon.document.mapper.SysUserMapper;
import online.keepon.document.util.BCryptPasswordEncoder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.io.IOException;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DocumentApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DocumentApplicationTests {
    @Resource
    private ArticleMapper articleMapper;
    @Autowired
    private SysUserMapper sysUserMapper;

    @Test
    public void contextLoads() throws IOException {
        RestTemplate restTemplate = new RestTemplate();
        String url = "https://cn.bing.com/HPImageArchive.aspx?format=js&idx=0&n=1&mkt=zh-CN";
        ResponseEntity<JSONObject> forEntity = restTemplate.getForEntity(url, JSONObject.class);
        if (forEntity.getStatusCode().equals(HttpStatus.OK) && forEntity.getBody() != null) {
            String image = "https://cn.bing.com" + forEntity.getBody().getJSONArray("images").getJSONObject(0).getString("url");
        }
    }

    @Test
    public void updateTest() {

        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String password = bCryptPasswordEncoder.encode("123456");
        sysUserMapper.updatePassword(password,"admin");
    }

}
