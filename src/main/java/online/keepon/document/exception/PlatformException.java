package online.keepon.document.exception;



/**
 * 消息
 *
 * @author top
 */
public class PlatformException extends RuntimeException {
    private static final long serialVersionUID = -2061435582341127950L;

    public PlatformException(String message) {
        super(message);
    }

    public PlatformException(String message, Throwable cause) {
        super(message, cause);
    }
}
