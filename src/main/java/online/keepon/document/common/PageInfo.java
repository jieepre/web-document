package online.keepon.document.common;

/**
 * @author fujie
 * @ 日期 2020-06-03 21:22
 */

import java.util.List;

/**
 * @author top
 */
public class PageInfo<T> {
    private int pageNum;
    private int pageSize;
    private int size;
    private int pages;
    private long total;
    private List<T> list;

    public PageInfo(List<T> list, int pageNum, int pageSize, long total) {
        this.list = list;
        if (list != null) {
            this.pageNum = (pageNum <= 0) ? 1 : pageNum;
            this.pageSize = (pageSize <= 0) ? 10 : pageSize;
            this.total = total;
            this.size = list.size();
            if (pageSize > 0) {
                this.pages = (int) (total / pageSize + ((total % pageSize == 0) ? 0 : 1));
            } else {
                this.pages = 0;
            }
            if (pageNum > this.pages) {
                if (pages != 0) {
                    this.pageNum = this.pages;
                }
            }
        }
    }

    public static class Page {
        private int pageNum;
        private int pageSize;
        private long total;

        public Page(int pageNum, int pageSize, long total) {
            this.pageNum = (pageNum <= 0) ? 1 : pageNum;
            this.pageSize = (pageSize <= 0) ? 10 : pageSize;
            this.total = total;
        }

        public int getPageNum() {
            return pageNum;
        }

        public void setPageNum(int pageNum) {
            this.pageNum = pageNum;
        }


        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getPageSize() {
            return pageSize;
        }

        public long getTotal() {
            return total;
        }

        public void setTotal(long total) {
            this.total = total;
        }

        private int getPages() {
            return (int) (total / pageSize + ((total % pageSize == 0) ? 0 : 1));
        }

        public static Page page(int pageNum, int pageSize, long total) {
            return new Page(pageNum, pageSize, total);
        }

        public int getOffset() {
            return getPages() == 0 ? 0 : (pageNum > getPages() ? (getPages() - 1) * pageSize : (pageNum - 1) * pageSize);
        }
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }
}