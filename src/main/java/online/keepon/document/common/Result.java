package online.keepon.document.common;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.ServletOutputStream;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * @author top
 */
@Slf4j
@Data
public class Result<T> {
    public Integer code;
    public String message;
    public String version = "1.0";
    public T payload;


    private Result<T> code(Integer code) {
        this.code = code;
        return this;
    }

    private Result<T> message(String message) {
        this.message = message;
        return this;
    }

    private Result<T> payload(T payload) {
        this.payload = payload;
        return this;
    }


    public static <T> Result<T> ok(ErrorInfo errorInfo, T payload) {
        return new Result<T>().code(errorInfo.code()).message(errorInfo.desc()).payload(payload);
    }

    public static <T> Result<T> ok(ErrorInfo errorInfo) {
        return new Result<T>().code(errorInfo.code()).message(errorInfo.desc()).payload(null);
    }

    public static <T> Result<T> ok(int code, String message, T payload) {
        return new Result<T>().code(code).message(message).payload(payload);
    }

    public static <T> Result<T> ok(T payload) {
        return new Result<T>().code(ErrorInfo.SUCCESS.code()).message(ErrorInfo.SUCCESS.desc()).payload(payload);
    }

    public static <T> Result<T> ok(String message) {
        return new Result<T>().code(ErrorInfo.SUCCESS.code()).message(message).payload(null);
    }

    public static <T> Result<T> fail(String message) {
        return new Result<T>().code(ErrorInfo.FAIL.code()).message(message).payload(null);
    }

    public static <T> Result<T> ok(int i) {
        if (i == 1) {
            return new Result<T>().code(ErrorInfo.SUCCESS.code()).message(ErrorInfo.SUCCESS.desc()).payload(null);
        }
        return new Result<T>().code(ErrorInfo.FAIL.code()).message(ErrorInfo.FAIL.desc()).payload(null);
    }

    public static void responseJson(ServletResponse servletResponse, ErrorInfo errorInfo) {
        ServletOutputStream out = null;
        try {
            HttpServletResponse response = (HttpServletResponse) servletResponse;
            response.reset();
            response.setCharacterEncoding("UTF-8");
            response.setHeader("Cache-Control", "no-cache");
            response.setContentType("application/json; charset=utf-8");
            response.setHeader("Access-Control-Allow-Origin", "*");
            out = response.getOutputStream();
            JSONObject object = new JSONObject(true);
            object.put("code", errorInfo.code());
            object.put("message", errorInfo.desc());
            object.put("payload", "");
            out.write(JSON.toJSONString(object).getBytes(StandardCharsets.UTF_8));
        } catch (Exception ignore) {
        } finally {
            if (out != null) {
                try {
                    out.flush();
                    out.close();
                } catch (IOException ignore) {
                }
            }
        }
    }
}
