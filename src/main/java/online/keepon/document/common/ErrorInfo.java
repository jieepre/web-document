package online.keepon.document.common;

/**
 * @author top
 */

public enum ErrorInfo {
    /**
     * success
     */
    SUCCESS(20000, "SUCCESS"),

    FAIL(40000, "失败"),

    UNAUTHENTICATION(40001, "未认证"),

    UNAUTHORIZED(40003, "没有权限"),


    /**
     * param error
     */
    PARAM_ERROR(61001, "参数错误"),

    /**
     * already exist
     */
    ALREADY_EXIST(61002, "已存在"),

    /**
     * not found in db
     */
    NOT_FOUND(61003, "not found"),

    /**
     * not exist
     */
    NOT_EXIST(61004, "not exist"),

    /**
     * no permission
     */
    NO_PERMISSION(61005, "没有权限"),

    /**
     * not register
     */
    NOT_REGISTRY(61006, "未注册"),

    /**
     * expires
     */
    EXPIRES(61007, "已过期"),

    /**
     * revoked
     */
    REVOKED(61008, "FAIL, revoked."),

    /**
     * serialized error
     */
    SERIALIZE_ERROR(61009, "serialized error."),


    /**
     * verify failed token已过期
     */
    VERIFY_FAILED(62001, "token expired"),
    /**
     * verify failed token校验失败
     */
    VERIFY_SIGN(62002, "token invalid"),


    /**
     * 重复提交
     */
    COMM_REPEAT(62003, "重复提交"),

    /**
     * error occur when operate file
     */
    FILE_ERROR(62004, "文件操作失败"),

    /**
     * error occur when operate db
     */
    DB_ERROR(62005, "保存失败"),

    /**
     * exception
     */
    EXCEPTION(50000, "未知错误"),
    /**
     * 平台异常
     */
    PLAT_EXCEPTION(50001, "系统异常");

    private final Integer errorCode;
    private final String errorDesc;

    ErrorInfo(Integer errorCode, String errorDesc) {
        this.errorCode = errorCode;
        this.errorDesc = errorDesc;
    }

    public Integer code() {
        return errorCode;
    }

    public String desc() {
        return errorDesc;
    }

}
