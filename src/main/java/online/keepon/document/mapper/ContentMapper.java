package online.keepon.document.mapper;

import online.keepon.document.model.Content;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ContentMapper {
    int insert(Content record);

    Content selectByPrimaryKey(Integer blogId);

    int update(Content content);
}
