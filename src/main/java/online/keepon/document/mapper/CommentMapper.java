package online.keepon.document.mapper;



import online.keepon.document.model.Comment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author top
 * @ 日期 2020-03-19 23:00
 */
@Mapper
public interface CommentMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Comment record);

    Comment selectByPrimaryKey(Integer id);

    int updateByPrimaryKey(Comment record);

    List<Comment> selectAllByPage(@Param("offset") int offset, @Param("pageSize") int pageSize);

    long countTotal();
}