package online.keepon.document.mapper;


import online.keepon.document.model.Article;
import online.keepon.document.model.CountFiltrate;
import online.keepon.document.model.dto.ArchivesNewsDTO;
import online.keepon.document.model.dto.ArticleItemDTO;
import online.keepon.document.model.dto.SearchDTO;
import online.keepon.document.model.dto.TagsDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author top
 * @ 日期 2020-03-19 23:01
 */
@Mapper
public interface ArticleMapper {
    int deleteByPrimaryKey(Integer id);

    int recoverByPrimaryKey(Integer id);

    int insert(Article record);

    Article selectByPrimaryKey(Integer id);

    int updateByPrimaryKey(Article record);

    int updateReadByID(@Param("read") Integer read, @Param("id") Integer id);

    List<Article> selectAllByPage(@Param("offset") int offset, @Param("pageSize") int pageSize, @Param("delete") Integer delete, @Param("status") Integer status);

    List<Article> selectArticlesNews();

    List<Article> selectAll();

    long countTotal(@Param("delete") Integer delete, @Param("status") Integer status);

    long searchCountTotal(@Param("search") SearchDTO search);

    List<Article> search(@Param("search") SearchDTO search, @Param("offset") int offset, @Param("pageSize") int pageSize);

    int articleIsTop(@Param("id") Integer id, @Param("top") Boolean top);

    CountFiltrate totalByFiltrate();

    List<ArchivesNewsDTO> selectArticleArchivesNews();

    List<TagsDTO> selectAllTags();

    long selectArticleArchivesCount();

    List<ArchivesNewsDTO> selectArticleArchives(@Param("offset") int offset, @Param("pageSize") int pageSize);

    long countCategoriesTotal(String categories);

    List<Article> selectAllCategoriesByPage(@Param("offset") int offset, @Param("pageSize") int pageSize, @Param("categories") String categories);

    long selectArticleArchivesConditionCount(@Param("condition") String condition);

    List<ArchivesNewsDTO> selectArticleArchivesConditionCountByPage(@Param("offset") int offset, @Param("pageSize") int pageSize, @Param("condition") String condition);

    ArticleItemDTO articleItemCount();

    long countTagsTotal(@Param("tags") String tags);

    List<Article> selectAllTagsByPage(@Param("offset") int offset, @Param("pageSize") int pageSize, @Param("tags") String tags);

    long countSearchTotal(@Param("title") String title);

    List<Article> selectAllSearchByPage(@Param("offset") int offset, @Param("pageSize") int pageSize, @Param("title") String title);

    List<Article> selectAllByStatus(String open);
}
