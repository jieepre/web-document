package online.keepon.document.mapper;


import online.keepon.document.model.SysUser;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @author top
 */
@Mapper
public interface SysUserMapper {
    /**
     * 添加一个用户
     *
     * @param sysUser 角色
     * @return 添加角色
     */
    @Options(useGeneratedKeys = true, keyColumn = "user_id", keyProperty = "userId")
    @Insert("insert into sys_user (username, password, status,create_time,update_time) VALUE(#{username},#{password},#{status} ,#{createTime} ,#{updateTime}  )")
    int insert(SysUser sysUser);


    /**
     * 查询用户
     *
     * @param userId userId
     * @return 用户
     */
    @Select("select user_id, username, password, status  ,create_time,update_time from sys_user  where user_id=#{userId} ")
    SysUser selectByPrimaryKey(Long userId);

    /**
     * 查询所有用户
     *
     * @return 用户
     */
    @Select("select user_id, username, status  ,create_time,update_time from sys_user")
    List<SysUser> selectAll();

    /**
     * 删除用户
     *
     * @param userId userId
     * @return 删除用户
     */
    @Delete("delete from sys_user  where user_id=#{userId} ")
    int deleteByPrimaryKey(Long userId);

    /**
     * 更新用户
     *
     * @param sysUser sysUser
     * @return 更新用户
     */
    @Update("update sys_user set username=#{username},status=#{status},update_time=#{updateTime}  where user_id=#{userId}")
    int updateByPrimaryKey(SysUser sysUser);



    /**
     * user
     *
     * @param username username
     * @return user
     */
    @Select("select user_id,username,password,status from sys_user where username=#{username}")
    SysUser selectUserByName(String username);


    /**
     * 统计用户
     *
     * @return 统计用户
     */
    @Select("select count(*) from sys_user")
    long countTotal();

    /**
     * 查询并分页
     *
     * @param offset 偏移量
     * @param size   分页数
     * @return 用户
     */
    @Select("select user_id, username, status ,create_time,update_time from sys_user  order by create_time desc limit #{offset},#{pageSize} ")
    List<SysUser> selectAllByPage(@Param("offset") int offset, @Param("pageSize") int pageSize);

    @Update("update sys_user set password=#{password} where username=#{username} ")
    int updatePassword(@Param("password") String password, @Param("username") String username);

}
