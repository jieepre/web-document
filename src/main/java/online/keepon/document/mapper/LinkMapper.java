package online.keepon.document.mapper;


import online.keepon.document.model.Link;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author top
 * @ 日期 2020-03-19 23:00
 */
@Mapper
public interface LinkMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Link record);

    Link selectByPrimaryKey(Integer id);

    int updateByPrimaryKey(Link record);

    long countTotal();

    List<Link> selectAllByPage(int offset, int pageSize);
}