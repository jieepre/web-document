package online.keepon.document.mapper;


import online.keepon.document.model.ArticleType;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author top
 * @ 日期 2020-03-19 23:00
 */
@Mapper
public interface ArticleTypeMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ArticleType record);

    ArticleType selectByPrimaryKey(Integer id);

    int updateByPrimaryKey(ArticleType record);

    long countTotal();

    List<ArticleType> selectAllByPage(@Param("offset") int offset, @Param("pageSize") int pageSize);

    List<ArticleType> selectCategories();

    long countCategoriesTotal();

    List<ArticleType> selectAllCategoriesByPage(@Param("offset") int offset, @Param("pageSize") int pageSize);
}