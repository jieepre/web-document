package online.keepon.document.controller;

import lombok.extern.slf4j.Slf4j;

import online.keepon.document.common.PageInfo;
import online.keepon.document.common.PageInfo.Page;
import online.keepon.document.common.Result;
import online.keepon.document.model.ArticleType;
import online.keepon.document.service.ArticleTypeService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author top
 * @ 日期 2020-03-22 9:00
 */
@Slf4j
@RestController
@RequestMapping(value = "/api/v1/document/type")
public class ArticleTypeController {
    @Resource
    private ArticleTypeService articleTypeService;

    @GetMapping(value = "/detail/{id}")
    public Result<ArticleType> type(@PathVariable Integer id) {
        ArticleType documentType = articleTypeService.selectByPrimaryKey(id);
        return Result.ok(documentType);
    }

    @DeleteMapping(value = "/delete/{id}")
    public Result<Integer> delete(@PathVariable Integer id) {
        int i = articleTypeService.deleteByPrimaryKey(id);
        return Result.ok(i);
    }

    @GetMapping(value = "/list/{page}/{size}")
    public Result<PageInfo<ArticleType>> list(@PathVariable Integer page, @PathVariable Integer size) {
        long total = articleTypeService.countTotal();
        PageInfo<ArticleType> documentType = articleTypeService.selectAllByPage(Page.page(page, size, total));
        return Result.ok(documentType);
    }

    @PostMapping(value = "/save")
    public Result<Integer> save(@RequestBody ArticleType type) {
        int i = articleTypeService.insert(type);
        return Result.ok(i);
    }

    @PutMapping(value = "/update")
    public Result<Integer> update(@RequestBody ArticleType type) {
        int i = articleTypeService.updateByPrimaryKey(type);
        return Result.ok(i);
    }
}
