package online.keepon.document.controller;

import lombok.extern.slf4j.Slf4j;

import online.keepon.document.common.PageInfo;
import online.keepon.document.common.PageInfo.Page;
import online.keepon.document.common.Result;
import online.keepon.document.model.Comment;
import online.keepon.document.service.CommentService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author top
 * @ 日期 2020-03-22 9:13
 */
@Slf4j
@RestController
@RequestMapping(value = "/api/v1/document/comment")
public class CommentController {

    @Resource
    private CommentService commentService;

    @GetMapping(value = "/detail/{id}")
    public Result<Comment> type(@PathVariable Integer id) {
        Comment comment = commentService.selectByPrimaryKey(id);
        return Result.ok(200, "Success", comment);
    }

    @GetMapping(value = "/list/{page}/{size}")
    public Result<PageInfo<Comment>> list(@PathVariable Integer page, @PathVariable Integer size) {
        long total = commentService.countTotal();
        PageInfo<Comment> documentType = commentService.selectAllByPage(Page.page(page, size, total));
        return Result.ok(200, "Success", documentType);
    }

    @PostMapping(value = "/save")
    public Result<Integer> save(@RequestBody Comment comment) {
        int i = commentService.insert(comment);
        return Result.ok(200, "Success", i);
    }

    @PutMapping(value = "/update")
    public Result<Integer> update(@RequestBody Comment comment) {
        int i = commentService.updateByPrimaryKey(comment);
        return Result.ok(200, "Success", i);
    }
}
