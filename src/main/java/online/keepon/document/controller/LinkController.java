package online.keepon.document.controller;

import lombok.extern.slf4j.Slf4j;

import online.keepon.document.common.PageInfo;
import online.keepon.document.common.PageInfo.Page;
import online.keepon.document.common.Result;
import online.keepon.document.model.Link;
import online.keepon.document.service.LinkService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author top
 * @ 日期 2020-03-22 9:13
 */
@Slf4j
@RestController
@RequestMapping(value = "/api/v1/document/link")
public class LinkController {

    @Resource
    private LinkService linkService;

    @GetMapping(value = "/detail/{id}")
    public Result<Link> type(@PathVariable Integer id) {
        Link link = linkService.selectByPrimaryKey(id);
        return Result.ok( link);
    }

    @GetMapping(value = "/list/{page}/{size}")
    public Result<PageInfo<Link>> list(@PathVariable Integer page, @PathVariable Integer size) {
        long total = linkService.countTotal();
        PageInfo<Link> pageInfo = linkService.selectAllByPage(Page.page(page, size, total));
        return Result.ok(pageInfo);
    }

    @PostMapping(value = "/save")
    public Result<Integer> save(@RequestBody Link link) {
        int i = linkService.insert(link);
        return Result.ok( i);
    }

    @PutMapping(value = "/update")
    public Result<Integer> update(@RequestBody Link link) {
        int i = linkService.updateByPrimaryKey(link);
        return Result.ok( i);
    }
}
