package online.keepon.document.controller;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

import online.keepon.document.common.PageInfo;
import online.keepon.document.common.PageInfo.Page;
import online.keepon.document.common.Result;
import online.keepon.document.model.Article;
import online.keepon.document.model.CountFiltrate;
import online.keepon.document.model.dto.ArticleDTO;
import online.keepon.document.model.dto.SearchDTO;
import online.keepon.document.service.ArticleService;
import org.springframework.web.bind.annotation.*;

/**
 * @author top
 */
@Slf4j
@RestController
@RequestMapping(value = "/api/v1/document")
public class ArticleController {

    private final ArticleService articleService;

    public ArticleController(ArticleService articleService) {
        this.articleService = articleService;
    }

    @ApiOperation(value = "详情")
    @GetMapping(value = "/article/detail/{id}")
    public Result<Article> document(@PathVariable String id) {
        Article document = articleService.selectByPrimaryKey(id);
        log.info("查看文章{}", id);
        return Result.ok(document);
    }

    @ApiOperation(value = "列表")
    @GetMapping(value = "/article/list/{page}/{size}")
    public Result<PageInfo<Article>> list(@PathVariable Integer page, @PathVariable Integer size, @RequestParam(defaultValue = "total") String status) {
        long total = articleService.countTotal(status);
        PageInfo<Article> document = articleService.selectAllByPage(Page.page(page, size, total),status);
        log.info("查看文章：总数{}，当前页：{},分页数：{}", total, document.getPageNum(), document.getPageSize());
        return Result.ok(document);
    }

    @PostMapping(value = "/article/save")
    public Result<?> save(@RequestBody ArticleDTO document) {
        int i = articleService.insert(document);
        return Result.ok(i);
    }

    @PutMapping(value = "/article/update")
    public Result<?> update(@RequestBody ArticleDTO document) {
        log.info("更新文章{}", document.getId());
        int i = articleService.updateByPrimaryKey(document);
        return Result.ok(i);
    }

    @DeleteMapping(value = "/article/recover/{id}")
    public Result<?> recover(@PathVariable String id) {
        int i = articleService.recoverByPrimaryKey(id);
        return Result.ok(i);
    }

    @DeleteMapping(value = "/article/delete/{id}")
    public Result<?> delete(@PathVariable String id) {
        int i = articleService.deleteByPrimaryKey(id);
        return Result.ok(i);
    }

    @PostMapping(value = "/article/search")
    public Result<?> search(@RequestBody SearchDTO search) {
        long total = articleService.searchCountTotal(search);
        PageInfo<Article> document = articleService.search(search, Page.page(search.getPage(), search.getSize(), total));
        log.info("查看文章：总数{}，当前页：{},分页数：{}", total, document.getPageNum(), document.getPageSize());
        return Result.ok(document);
    }

    @PostMapping(value = "/article/top/{id}")
    public Result<?> top(@PathVariable String id, @RequestParam Boolean top) {
        int i = articleService.articleIsTop(id, top);
        return Result.ok(i);
    }

    @GetMapping(value = "/article/total")
    public Result<CountFiltrate> total() {
        return Result.ok(articleService.totalByFiltrate());
    }
}
