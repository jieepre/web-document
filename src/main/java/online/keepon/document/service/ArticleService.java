package online.keepon.document.service;


import online.keepon.document.common.PageInfo;
import online.keepon.document.common.PageInfo.Page;
import online.keepon.document.model.Article;
import online.keepon.document.model.CountFiltrate;
import online.keepon.document.model.dto.*;

import java.util.List;

/**
 * @author fujie
 * @ 日期 2020-03-19 23:01
 */

public interface ArticleService {


    int deleteByPrimaryKey(String id);

    int recoverByPrimaryKey(String id);

    int insert(ArticleDTO record);

    Article selectByPrimaryKey(String hid);

    int updateByPrimaryKey(ArticleDTO record);

    long countTotal(String status);

    PageInfo<Article> selectAllByPage(Page page, String status);

    long searchCountTotal(SearchDTO search);

    PageInfo<Article> search(SearchDTO search, Page page);

    int articleIsTop(String id, Boolean top);

    CountFiltrate totalByFiltrate();

    List<Article> selectArticlesNews();

    List<ArchivesNewsDTO> selectArticleArchivesNews();

    List<TagsDTO> selectAllTags();

    long selectArticleArchivesCount();

    PageInfo<ArchivesNewsDTO> selectArticleArchives(Page page);

    long countCategoriesTotal(String categories);

    PageInfo<Article> selectAllCategoriesByPage(Page page, String categories);

    long selectArticleArchivesConditionCount(String condition);

    PageInfo<ArchivesNewsDTO> selectArticleArchivesCondition(Page page, String condition);

    ArticleItemDTO articleItemCount();

    long countTagsTotal(String tags);

    PageInfo<Article> selectAllTagsByPage(Page page, String tags);

    long countSearchTotal(String search);

    PageInfo<Article> selectAllSearchByPage(Page page, String search);

    List<Article> selectAll(String open);
}
