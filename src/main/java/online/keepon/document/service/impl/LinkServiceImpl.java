package online.keepon.document.service.impl;


import online.keepon.document.common.PageInfo;
import online.keepon.document.common.PageInfo.Page;
import online.keepon.document.mapper.LinkMapper;
import online.keepon.document.model.Link;
import online.keepon.document.service.LinkService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author fujie
 * @ 日期 2020-03-19 23:00
 */

@Service
public class LinkServiceImpl implements LinkService {

    @Resource
    private LinkMapper linkMapper;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return linkMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Link record) {
        return linkMapper.insert(record);
    }

    @Override
    public Link selectByPrimaryKey(Integer id) {
        return linkMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKey(Link record) {
        return linkMapper.updateByPrimaryKey(record);
    }

    @Override
    public long countTotal() {
        return linkMapper.countTotal();
    }

    @Override
    public PageInfo<Link> selectAllByPage(Page page) {
        List<Link> links = linkMapper.selectAllByPage(page.getOffset(), page.getPageSize());
        return new PageInfo<>(links, page.getPageNum(), page.getPageSize(), page.getTotal());
    }
}
