package online.keepon.document.service.impl;


import online.keepon.document.common.PageInfo;
import online.keepon.document.common.PageInfo.Page;
import online.keepon.document.mapper.ContentMapper;
import online.keepon.document.model.ArticleType;
import online.keepon.document.model.Content;
import online.keepon.document.service.ContentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class ContentServiceImpl implements ContentService {

    @Resource
    private ContentMapper contentMapper;

    @Override
    public int insert(Content record) {
        return contentMapper.insert(record);
    }

    @Override
    public long countTotal() {
        return 0;
    }

    @Override
    public Content selectContentByArticleId(Integer articleId) {
        return contentMapper.selectByPrimaryKey(articleId);
    }

}




