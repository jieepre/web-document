package online.keepon.document.service.impl;


import online.keepon.document.common.PageInfo;
import online.keepon.document.common.PageInfo.Page;
import online.keepon.document.mapper.CommentMapper;
import online.keepon.document.model.Comment;
import online.keepon.document.service.CommentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author fujie
 * @ 日期 2020-03-19 23:00
 */

@Service
public class CommentServiceImpl implements CommentService {

    @Resource
    private CommentMapper commentMapper;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return commentMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Comment record) {
        return commentMapper.insert(record);
    }

    @Override
    public Comment selectByPrimaryKey(Integer id) {
        return commentMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKey(Comment record) {
        return commentMapper.updateByPrimaryKey(record);
    }

    @Override
    public long countTotal() {
        return commentMapper.countTotal();
    }

    @Override
    public PageInfo<Comment> selectAllByPage(Page page) {
        List<Comment> comments = commentMapper.selectAllByPage(page.getOffset(), page.getPageSize());
        return new PageInfo<>(comments, page.getPageNum(), page.getPageSize(), page.getTotal());
    }
}
