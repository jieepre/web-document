package online.keepon.document.service.impl;


import online.keepon.document.common.PageInfo;
import online.keepon.document.common.PageInfo.Page;
import online.keepon.document.common.Result;
import online.keepon.document.exception.PlatformException;
import online.keepon.document.mapper.SysUserMapper;
import online.keepon.document.model.SysUser;
import online.keepon.document.model.dto.ModifyPassDTO;
import online.keepon.document.service.SysUserService;
import online.keepon.document.util.BCryptPasswordEncoder;
import online.keepon.document.util.DateUtil;
import online.keepon.document.util.FileUtil;
import online.keepon.document.util.RsaUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;


/**
 * @author top
 */
@Service("sysUserService")
public class SysUserServiceImpl implements SysUserService {

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    /**
     * 根据用户名查询实体
     */
    @Override
    public SysUser selectUserByName(String username) {
        return this.sysUserMapper.selectUserByName(username);
    }



    @Override
    public int save(SysUser user) {
        user.setPassword(bCryptPasswordEncoder.encode("123456"));
        user.setCreateTime(DateUtil.toEpochSecond(LocalDateTime.now()));
        user.setUpdateTime(DateUtil.toEpochSecond(LocalDateTime.now()));
        return sysUserMapper.insert(user);
    }

    @Override
    public int update(SysUser user) {
        if (user.getUserId().equals(1L)) {
            throw new PlatformException("禁止该操作");
        }
        user.setUpdateTime(DateUtil.toEpochSecond(LocalDateTime.now()));
        return sysUserMapper.updateByPrimaryKey(user);
    }

    @Override
    public int deleteByPrimaryKey(Long id) {
        if (id.equals(1L)) {
            throw new PlatformException("禁止该操作");
        }
        return sysUserMapper.deleteByPrimaryKey(id);
    }

    @Override
    public SysUser selectByPrimaryKey(Long id) {
        return sysUserMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<SysUser> selectAll() {
        return sysUserMapper.selectAll();
    }



    @Override
    public long countTotal() {
        return sysUserMapper.countTotal();
    }

    @Override
    public PageInfo<SysUser> selectAllByPage(Page page) {
        List<SysUser> sysUsers = sysUserMapper.selectAllByPage(page.getOffset(), page.getPageSize());
        return new PageInfo<>(sysUsers, page.getPageNum(), page.getPageSize(), page.getTotal());
    }

    @Override
    public Result<?> updatePassword(ModifyPassDTO user) throws Exception {
        SysUser sysUser = selectUserByName("");
        String password = RsaUtil.decrypt(user.getOldPassword(), RsaUtil.getPrivateKey(FileUtil.getPrivateKey()));
        boolean matches = bCryptPasswordEncoder.matches(password, sysUser.getPassword());
        if (!matches) {
            return Result.fail("原密码错误");
        }
        String newPassword = RsaUtil.decrypt(user.getNewPassword(), RsaUtil.getPrivateKey(FileUtil.getPrivateKey()));
        int i = sysUserMapper.updatePassword(bCryptPasswordEncoder.encode(newPassword), sysUser.getUsername());
        return Result.ok(i);
    }
}
