package online.keepon.document.service.impl;


import online.keepon.document.common.PageInfo;
import online.keepon.document.common.PageInfo.Page;
import online.keepon.document.mapper.ArticleTypeMapper;
import online.keepon.document.model.ArticleType;
import online.keepon.document.service.ArticleTypeService;
import online.keepon.document.util.DateUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author fujie
 * @ 日期 2020-03-19 23:00
 */

@Service
public class ArticleTypeServiceImpl implements ArticleTypeService {

    @Resource
    private ArticleTypeMapper articleTypeMapper;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return articleTypeMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(ArticleType record) {
        record.setCreateTime(DateUtil.toEpochSecond(LocalDateTime.now()));
        record.setUpdateTime(DateUtil.toEpochSecond(LocalDateTime.now()));
        return articleTypeMapper.insert(record);
    }

    @Override
    public ArticleType selectByPrimaryKey(Integer id) {
        return articleTypeMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKey(ArticleType record) {
        record.setUpdateTime(DateUtil.toEpochSecond(LocalDateTime.now()));
        return articleTypeMapper.updateByPrimaryKey(record);
    }

    @Override
    public long countTotal() {
        return articleTypeMapper.countTotal();
    }

    @Override
    public PageInfo<ArticleType> selectAllByPage(Page page) {
        List<ArticleType> blogTypes = articleTypeMapper.selectAllByPage(page.getOffset(), page.getPageSize());
        return new PageInfo<>(blogTypes, page.getPageNum(), page.getPageSize(), page.getTotal());
    }

    @Override
    public List<ArticleType> selectCategories() {
        return articleTypeMapper.selectCategories();
    }

    @Override
    public long countCategoriesTotal() {
        return articleTypeMapper.countCategoriesTotal();
    }

    @Override
    public PageInfo<ArticleType> selectAllCategoriesByPage(Page page) {
        List<ArticleType> articleTypeList = articleTypeMapper.selectAllCategoriesByPage(page.getOffset(), page.getPageSize());
        return new PageInfo<>(articleTypeList, page.getPageNum(), page.getPageSize(), page.getTotal());
    }
}
