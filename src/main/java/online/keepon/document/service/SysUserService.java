package online.keepon.document.service;


import online.keepon.document.common.PageInfo;
import online.keepon.document.common.PageInfo.Page;
import online.keepon.document.common.Result;
import online.keepon.document.model.SysUser;
import online.keepon.document.model.dto.ModifyPassDTO;

import java.util.List;

/**
 * @author top
 */
public interface SysUserService {

    /**
     * 根据用户名查询实体
     *
     * @param username username
     * @return 用户
     */
    SysUser selectUserByName(String username);


    int save(SysUser user);

    int update(SysUser user);

    int deleteByPrimaryKey(Long id);

    SysUser selectByPrimaryKey(Long id);

    List<SysUser> selectAll();



    long countTotal();

    PageInfo<SysUser> selectAllByPage(Page page);

    Result<?> updatePassword(ModifyPassDTO user) throws Exception;
}
