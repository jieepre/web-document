package online.keepon.document.service;


import online.keepon.document.model.Content;

public interface ContentService {


    int insert(Content record);

    long countTotal();

    Content selectContentByArticleId(Integer articleId);

}




