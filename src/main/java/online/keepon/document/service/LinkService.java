package online.keepon.document.service;


import online.keepon.document.common.PageInfo;
import online.keepon.document.common.PageInfo.Page;
import online.keepon.document.model.Link;

/**
*@author fujie
*@ 日期 2020-03-19 23:00
*/

public interface LinkService{


    int deleteByPrimaryKey(Integer id);

    int insert(Link record);

    Link selectByPrimaryKey(Integer id);

    int updateByPrimaryKey(Link record);


    long countTotal();

    PageInfo<Link> selectAllByPage(Page page);

}
