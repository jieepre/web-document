package online.keepon.document.service;


import online.keepon.document.common.PageInfo;
import online.keepon.document.common.PageInfo.Page;
import online.keepon.document.model.Comment;

/**
 * @author fujie
 * @ 日期 2020-03-19 23:00
 */

public interface CommentService {


    int deleteByPrimaryKey(Integer id);

    int insert(Comment record);

    Comment selectByPrimaryKey(Integer id);

    int updateByPrimaryKey(Comment record);

    long countTotal();

    PageInfo<Comment> selectAllByPage(Page page);
}
