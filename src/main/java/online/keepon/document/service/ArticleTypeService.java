package online.keepon.document.service;


import online.keepon.document.common.PageInfo;
import online.keepon.document.common.PageInfo.Page;
import online.keepon.document.model.ArticleType;

import java.util.List;

/**
 * @author fujie
 * @ 日期 2020-03-19 23:00
 */

public interface ArticleTypeService {


    int deleteByPrimaryKey(Integer id);

    int insert(ArticleType record);

    ArticleType selectByPrimaryKey(Integer id);

    int updateByPrimaryKey(ArticleType record);

    long countTotal();

    PageInfo<ArticleType> selectAllByPage(Page page);

    List<ArticleType> selectCategories();

    PageInfo<ArticleType> selectAllCategoriesByPage(Page page);

    long countCategoriesTotal();
}
