package online.keepon.document.util;


import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.io.*;

/**
 * @author top
 * @ 日期 2019-06-05 14:04
 */
@Slf4j
public class FileUtil {
    public static void upload(@NotNull byte[] file, @NotNull String name, @NotNull String path) {
        File f = new File(path);
        if (!f.exists()) {
            boolean mkdirs = f.mkdirs();
        }
        try {
            FileOutputStream out = new FileOutputStream(path + File.separator + name);
            out.write(file);
            out.flush();
            out.close();
        } catch (IOException e) {
            log.info("图片上传异常:【{}】", e.getMessage(), e);
        }
    }

    public static String getPrivateKey() throws IOException {
        ClassPathResource privateKey = new ClassPathResource("cer/private.key");
        InputStream inputStream = privateKey.getInputStream();
        BufferedInputStream bis = new BufferedInputStream(inputStream);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int c = bis.read();
        while (c != -1) {
            baos.write(c);
            c = bis.read();
        }
        bis.close();
        byte[] retArr = baos.toByteArray();
        return new String(retArr);
       // return privateKeycode.replaceAll("-----BEGIN PRIVATE KEY-----", "").replaceAll("-----END PRIVATE KEY-----", "");
    }

    public static String getPublicKey() throws IOException {
        ClassPathResource publicKey = new ClassPathResource("cer/public.key");
        BufferedReader publicKerfuffleReader = new BufferedReader(new FileReader(publicKey.getFile()));
        StringBuilder publicKeycode = new StringBuilder();
        String publicKeeLine;
        while ((publicKeeLine = publicKerfuffleReader.readLine()) != null) {
            publicKeycode.append(publicKeeLine);
        }
        return publicKeycode.toString();
//        return publicKeycode.toString().replaceAll("-----BEGIN PUBLIC KEY-----", "").replaceAll("-----END PUBLIC KEY-----", "");
    }

    public static void download(HttpServletResponse response, String filepath, String filename) {
        FileSystemResource fileSystemResource = new FileSystemResource(filepath);
        File file = fileSystemResource.getFile();
        if (file.exists()) {
            log.info("已找到Excel表格【{}.xls】", filename);
            log.info("文件存放路径【{}】", filepath + File.separator + filename + ".key");
            response.setContentType("application/force-download");
            response.addHeader("Content-Disposition", "attachment;fileName=" + filename + ".key");
            byte[] buffer = new byte[1024];
            FileInputStream fis = null;
            BufferedInputStream bis = null;
            try {
                fis = new FileInputStream(file);
                bis = new BufferedInputStream(fis);
                OutputStream os = response.getOutputStream();
                int i = bis.read(buffer);
                while (i != -1) {
                    os.write(buffer, 0, i);
                    i = bis.read(buffer);
                }
                log.info("下载成功【{}】", filename);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bis != null) {
                    try {
                        bis.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (fis != null) {
                    try {
                        fis.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
