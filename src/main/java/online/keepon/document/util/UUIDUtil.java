package online.keepon.document.util;

import java.util.UUID;

/**
 * @author top
 */
public class UUIDUtil {

    public static String uuid() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    public static String uuid2() {
        return uuid() + uuid();
    }

}
