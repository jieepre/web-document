package online.keepon.document.site;

import lombok.extern.slf4j.Slf4j;
import online.keepon.document.model.Article;
import online.keepon.document.service.ArticleService;
import online.keepon.document.util.Hashids;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

@Slf4j
@Component
public class BaiduSitemap {
    @Autowired
    private ArticleService articleService;

    private static final Hashids hashids = new Hashids("algorithm_salt", 8);

    public String encode(Integer id) {
        return hashids.encode(id);
    }

    @Scheduled(cron = " 0 0 21 * * ? ")
    public void schedulingPush() {
        List<Article> articles = articleService.selectAll("open");
        if (!articles.isEmpty()) {
            for (Article article : articles) {
                // 网站的服务器连接
                String url = "http://data.zz.baidu.com/urls?site=https://www.keepon.online&token=2q3MrIFADJTKQbOk";
                // 执行推送方法
                String json = Post(url, "https://www.keepon.online/posts/" + encode(article.getId()));
                // 打印推送结果
                log.info("结果是" + json);
            }
        }


    }

    /**
     * 百度链接实时推送
     *
     * @param PostUrl    url
     * @param Parameters urls
     * @return 推送结果
     */
    public static String Post(String PostUrl, String Parameters) {
        if (null == PostUrl || null == Parameters || Parameters.equals("")) {
            return null;
        }
        StringBuilder result = new StringBuilder();
        PrintWriter out = null;
        BufferedReader in = null;
        try {
            //建立URL之间的连接
            URLConnection conn = new URL(PostUrl).openConnection();
            //设置通用的请求属性
            conn.setRequestProperty("Host", "data.zz.baidu.com");
            conn.setRequestProperty("User-Agent", "curl/7.12.1");
            conn.setRequestProperty("Content-Length", "83");
            conn.setRequestProperty("Content-Type", "text/plain");

            //发送POST请求必须设置如下两行
            conn.setDoInput(true);
            conn.setDoOutput(true);

            //获取conn对应的输出流
            out = new PrintWriter(conn.getOutputStream());
            //发送请求参数
//            StringBuilder param = new StringBuilder();
//            for (String s : Parameters) {
//                param.append(s).append("\n");
//            }
            out.print(Parameters.trim());
            //进行输出流的缓冲
            out.flush();
            //通过BufferedReader输入流来读取Url的响应
            in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                result.append(line);
            }
        } catch (Exception e) {
            System.out.println("发送post请求出现异常！" + e);
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return result.toString();
    }
}
