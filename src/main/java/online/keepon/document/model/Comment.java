package online.keepon.document.model;

import lombok.Data;

import java.io.Serializable;


@Data
public class Comment implements Serializable {

    private static final long serialVersionUID = 8102000923427519379L;
    /**
     * id (无默认值)
     */
    private Integer id;

    /**
     * 用户ip (无默认值)
     */
    private String userIp;

    /**
     * 博客id (无默认值)
     */
    private Integer articleId;
    /**
     * 评论内容 (无默认值)
     */
    private String content;

    /**
     * 评论时间 (无默认值)
     */
    private Long commentDate;

    /**
     * 状态 (无默认值)
     * 审核状态  0 待审核 1 审核通过 2 审核未通过
     */
    private Integer status;

}