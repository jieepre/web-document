package online.keepon.document.model;

import lombok.Data;

import java.io.Serializable;


@Data
public class ArticleType implements Serializable {
    private static final long serialVersionUID = 7120599096544754015L;
    private Integer id;
    private String categories;
    private String description;
    private Integer articleCount;
    private Boolean show;
    private Long createTime;
    private Long updateTime;

}
