package online.keepon.document.model;

import lombok.Data;

/**
 * @author fujie
 * @ 日期 2020-06-11 0:16
 */
@Data
public class CountFiltrate {
    private Integer total;
    private Integer open;
    private Integer secret;
    private Integer draft;
    private Integer recycle;
}
