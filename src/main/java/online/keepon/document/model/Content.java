package online.keepon.document.model;

import lombok.Data;

import java.io.Serializable;

/**
 * @author top
 */
@Data
public class Content implements Serializable {
    private static final long serialVersionUID = -8805071656634322245L;
    private Integer articleId;
    private String content;
}
