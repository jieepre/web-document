package online.keepon.document.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.io.Serializable;


@Data
public class Article implements Serializable {
    private static final long serialVersionUID = 8083160632204512040L;

    @JsonIgnore
    private Integer id;
    /**
     * hashids
     */
    private String hid;
    /**
     * 博客标题 (无默认值)
     */

    private String title;

    /**
     * 摘要 (无默认值)
     */

    private String summary;

    /**
     * 发布日期 (无默认值)
     */

    private Long createTime;

    private Long updateTime;

    /**
     * 查看次数 (无默认值)
     */

    private Integer read = 0;

    /**
     * 回复次数 (无默认值)
     */

    private Integer reply = 0;

    /**
     * 博客类型 (无默认值)
     */

    private String type;

    private Integer categoryId;

    /**
     * 关键字 空格隔开 (无默认值)
     */
    private String keyword;

    private Integer status;

    private String images;
    /**
     * 置顶
     */
    private Integer top;

    private Integer del;
    private String wordTotal;
    private String readTime;

    /**
     * 博客内容 (无默认值)
     */
    private String content;

    private String categories;
}
