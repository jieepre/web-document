package online.keepon.document.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class Link implements Serializable {

    private static final long serialVersionUID = 610971683518335773L;
    /**
     * (无默认值)
     */
    private Integer id;

    /**
     * (无默认值)
     */
    private String linkName;

    /**
     * (无默认值)
     */
    private String linkUrl;
}