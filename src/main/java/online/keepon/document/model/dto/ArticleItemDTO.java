package online.keepon.document.model.dto;

import lombok.Data;

@Data
public class ArticleItemDTO {
    private Integer tags;
    private Integer article;
    private Integer category;
    private Integer wordTotal;
}
