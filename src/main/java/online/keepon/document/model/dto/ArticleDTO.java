package online.keepon.document.model.dto;

import lombok.Data;

import java.io.Serializable;


@Data
public class ArticleDTO implements Serializable {
    private static final long serialVersionUID = 8083160632904512040L;
    private String hid;
    private Integer id;

    /**
     * 博客标题 (无默认值)
     */

    private String title;

    /**
     * 摘要 (无默认值)
     */

    private String summary = "";


    private String content;


    /**
     * 博客类型 (无默认值)
     */

    private String type;

    private Integer categoryId;

    /**
     * 关键字 空格隔开 (无默认值)
     */
    private String keyword;

    private String images;

    private Integer status;
    private String wordTotal;
    private String readTime;

}
