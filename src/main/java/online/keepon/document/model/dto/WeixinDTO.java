package online.keepon.document.model.dto;

import lombok.Data;

@Data
public class WeixinDTO {
    private String openid;
    private String avatarUrl;
    private String city;
    private String country;
    private Integer gender;
    private String language;
    private String nickName;
    private String province;
}
