package online.keepon.document.model.dto;

import lombok.Data;

@Data
public class TagsDTO {
    private Integer count;
    private String tag;
}
