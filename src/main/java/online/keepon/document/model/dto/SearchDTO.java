package online.keepon.document.model.dto;

import lombok.Data;

/**
 * @author fujie
 * @ 日期 2020-06-09 22:59
 */
@Data
public class SearchDTO {
    private String keywords;
    private Integer year;
    private Integer month;
    private String type;
    private Integer column;
    private Integer status;
    private Integer page;
    private Integer size;

    public String getKeywords() {
        return keywords.trim();
    }
}
