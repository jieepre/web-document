package online.keepon.document.model.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ModifyPassDTO {
    @NotBlank
    private String newPassword;
    @NotBlank
    private String oldPassword;
}
