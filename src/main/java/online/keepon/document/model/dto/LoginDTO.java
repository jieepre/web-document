package online.keepon.document.model.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
public class LoginDTO implements Serializable {
    @NotBlank
    private String username;
    @NotBlank
    private String password;
}
