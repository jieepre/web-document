package online.keepon.document.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class ArchivesNewsDTO {
    @JsonIgnore
    private Integer id;
    private String hid;
    private String title;
    private Integer count;
    private String archives;
}
