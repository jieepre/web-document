#!/bin/bash
mvn clean package -Dmaven.test.skip=true
cd target || exit
docker build -f ../docker/Dockerfile-backend -t jieepre/document:20200615 .
